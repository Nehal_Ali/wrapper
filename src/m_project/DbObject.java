
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m_project;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public abstract class DbObject {

    private RelationalMapper DbCon;
    private String tableName = this.getClass().getSimpleName();
    ArrayList<String> fieldNames, fieldTypes;
    private Field[] fields;
    RelationalMapper myDB;

    DbObject() {

    }

    public void save() {
        //Class c = this.getClass();
        //fields = c.getDeclaredFields();

        createDB();
        insertRow();
       
    }

   

    public ArrayList<DbObject> findAll() {
        //Class c = this.getClass();
        //fields = c.getDeclaredFields();
        createDB();
        String DbOutput = myDB.selectAll(tableName, fieldNames);

        String[] rows = DbOutput.split("\n");
        ArrayList<DbObject> childObjects = new ArrayList();

        for (int i = 0; i < rows.length; i++) {
            try {
                childObjects.add(this.getClass().getDeclaredConstructor().newInstance());
            } catch (Exception ex) {
                Logger.getLogger(DbObject.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        for (int j = 0; j < rows.length; j++) {
            String[] myRow = rows[j].split(" ");
            for (int i = 0; i < fields.length; i++) {
                Object myValue = parser(fields[i].getType().getSimpleName(), myRow[i]);
                runSetter(fields[i], childObjects.get(j), myValue);
            }
        }

        return childObjects;
    }

    public DbObject findByID(int ID) {
        createDB();
        DbObject child = null;
        
        try {
            child = this.getClass().getDeclaredConstructor().newInstance();
        } catch (Exception ex) {
            Logger.getLogger(DbObject.class.getName()).log(Level.SEVERE, null, ex);
        }

        String DbOutput = myDB.selectByID(tableName, fieldNames, ID);

        String[] myRow = DbOutput.split(" ");

        for (int i = 0; i < myRow.length; i++) {
            Object myValue = parser(fields[i].getType().getSimpleName(), myRow[i]);
            runSetter(fields[i], this, myValue);
        }

        return child;
    }

    private void createDB() {
        Class c = this.getClass();
        fields = c.getDeclaredFields();

        fieldNames = new ArrayList();
        fieldTypes = new ArrayList();

        for (Field field : fields) {
            fieldNames.add(field.getName());
            fieldTypes.add(field.getType().getSimpleName());
        }

        try {
            myDB = new RelationalMapper(tableName, fieldNames, fieldTypes);
        } catch (SQLException ex) {
            Logger.getLogger(DbObject.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void insertRow() {
        ArrayList<Object> fieldVals = new ArrayList();
        updateRowNow();
        
        for (Field field : fields) {
            Object myVal = runGetter(field, this);
            fieldVals.add(myVal);
            
        }
        
              System.out.println("Values are being created..\n");
               myDB.insert(fieldVals, tableName);
              
        
    }
 private void updateRowNow() {
              //System.out.println("Values already exist..\nUpdating Values..");
        ArrayList<Object> fieldVals = new ArrayList();

        for (Field field : fields) {
            Object myVal = runGetter(field, this);
            fieldVals.add(myVal);
        }
       // System.out.println("Values Already exist\n");
             myDB.updateRow(fieldVals, tableName);
          
            
    }
    private static Object runGetter(Field field, Object o) {
        // MZ: Find the correct method
        for (Method method : o.getClass().getDeclaredMethods()) {
            if ((method.getName().startsWith("get")) && (method.getName().length() == (field.getName().length() + 3))) {
                if (method.getName().toLowerCase().endsWith(field.getName().toLowerCase())) {
                    try {
                        return method.invoke(o);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        System.out.println("Could not determine method: " + method.getName());
                    }

                }
            }
        }

        return null;
    }

    private static Object runSetter(Field field, Object o, Object Value) {
        // MZ: Find the correct method
        for (Method method : o.getClass().getDeclaredMethods()) {
            if ((method.getName().startsWith("set")) && (method.getName().length() == (field.getName().length() + 3))) {
                if (method.getName().toLowerCase().endsWith(field.getName().toLowerCase())) {
                    try {
                        return method.invoke(o, Value);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        System.out.println("Could not determine method: " + method.getName());
                    }

                }
            }
        }

        return null;
    }

    
    private Object parser(String type, String Value) {
        switch (type) {
            case "short":
                return Short.parseShort(Value);
            case "int":
                return Integer.parseInt(Value);
            case "double":
                return Double.parseDouble(Value);
            case "long":
                return Long.parseLong(Value);
            case "float":
                return Float.parseFloat(Value);
            case "boolean":
                return Boolean.parseBoolean(Value);
            case "String":
                return Value;
            case "byte":
                return Byte.parseByte(Value);
        }
        return null;
    }


}
