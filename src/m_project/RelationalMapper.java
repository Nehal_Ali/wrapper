package m_project;

import java.sql.*;
import static java.lang.System.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RelationalMapper {

    private Connection connection;
    private Statement statement;
    Map <String, String> typeMap = new HashMap <String, String>();
    ArrayList<String> fieldNames, fieldTypes;
    
    public RelationalMapper(String tableName, ArrayList<String> fieldNames, ArrayList<String> fieldTypes)
            throws SQLException {
        this.fieldNames = fieldNames;
        this.fieldTypes = fieldTypes;
        
        String username = "root";
        String password = "";
        String dbName = "mydb";
        String url = "127.0.0.1";
        
        connection = DriverManager.getConnection(
                "jdbc:mysql://" + url + ":3306/" + dbName,
                username, password);
        Statement st = connection.createStatement();
        
        initMap();  
        
        String init = "";
        for (int i = 0; i < fieldNames.size(); i++) {
            init += fieldNames.get(i) + " ";
            init += typeMap.get(fieldTypes.get(i));
            if(i!= fieldNames.size() - 1)
                init += ", ";
        }
        String command = "create table if not exists " + tableName + "(" +
                "id " + "integer primary key auto_increment, " + init + ");";
        st.executeUpdate(command);
    
    }
    
    private void initMap(){
        typeMap.put("String", "VARCHAR(20)");
        typeMap.put("boolean", "BIT");
        typeMap.put("byte", "TINYINT");
        typeMap.put("short", "SMALLINT");
        typeMap.put("int", "INTEGER");
        typeMap.put("long", "BIGINT");
        typeMap.put("float", "REAL");
        typeMap.put("double", "DOUBLE");
    }

    public String selectAll(String tableName, ArrayList<String> fields) {
        String selectStatement = "SELECT * "
                + "FROM " + tableName + " ";
        String ret = "";

        try {
            statement = connection.createStatement();
            ResultSet result = statement.executeQuery(selectStatement);

            while (result.next()) {
                for (String field : fields) {
                    String currentFieldValue = result.getString(field);

                    if (currentFieldValue != null) {
                        ret += result.getString(field) + " ";
                    }
                }

                ret = ret.substring(0, ret.length() - 1) + "\n";
            }

        } catch (SQLException e) {
            err.println(createSqlExceptionInfo(e));
        } finally {
            resetStatement();
        }

        return ret;
    }

    public String selectByID(String tableName, ArrayList<String> fields, int ID) {
        String selectStatement = "SELECT * "
                + "FROM " + tableName + " where id=" + ID;
        String ret = "";
        
        try {
            statement = connection.createStatement();
            ResultSet result = statement.executeQuery(selectStatement);
            
            result.next();
            for (String field : fields) {
                String currentFieldValue = result.getString(field);

                if (currentFieldValue != null) {
                    ret += result.getString(field) + " ";
                }
            }
            ret = ret.substring(0, ret.length() - 1);

        } catch (SQLException e) {
            err.println(createSqlExceptionInfo(e));
        } finally {
            resetStatement();
        }

        return ret;
    }
/*public String selectByCon(ArrayList<Object> myVals, String tableName) {
            String ret = "";
        try {
            statement = connection.createStatement();
            
            String columnNames = "";
            String Values = "";
            
            String sqlSel = "SELECT * FROM "+tableName+" WHERE ";
            
            for(int i = 0 ; i < myVals.size() ; i++){
               if(fieldTypes.get(i).equals("String")){
                    Values += "'" + myVals.get(i) + "'";
                    columnNames = "`"+fieldNames.get(i)+"`";
                sqlSel+=columnNames+" = '"+myVals.get(i)+"'";
                  
                
                
                 
                }
                else{
                    Values += myVals.get(i);
                   columnNames = "`"+fieldNames.get(i)+"`";
                sqlSel+=columnNames+" = "+myVals.get(i);
                  
                }
                if(i!= myVals.size() - 1)
                {
                    sqlSel+=" AND ";
                    Values += ", ";
                    columnNames +=", ";
                }
            }
             sqlSel+=  " ;";//+columnNames+" = "+Values+" ;";
            
            ResultSet result = statement.executeQuery(sqlSel);
            result.next();
            for (String field : myVals) {
                String currentFieldValue = result.getString(field);

                if (currentFieldValue != null) {
                    ret += result.getString(field) + " ";
                }
            }
            ret = ret.substring(0, ret.length() - 1);
            System.out.println(sqlSel+"\n"+Values+"\n"+columnNames);
           System.out.println(statement.executeQuery(sqlSel)); statement.executeQuery(sqlSel);
        } catch (SQLException e) {
            err.println(createSqlExceptionInfo(e));
        } finally {
            resetStatement();
        }
        return ret;
}*/
    public void insert(ArrayList<Object> myVals, String tableName) {
        try {
            statement = connection.createStatement();
            
            String columnNames = "";
            String Values = "";
            for(int i = 0 ; i < myVals.size() ; i++){
                columnNames += fieldNames.get(i);
                
                if(fieldTypes.get(i).equals("String"))
                    Values += "'" + myVals.get(i) + "'";
                else
                    Values += myVals.get(i);
                
                if(i!= myVals.size() - 1)
                {
                    Values += ", ";
                    columnNames +=", ";
                }
            }
            
            String sqlInsert = "insert into " + tableName + " (" + columnNames + ") "
                    + "values (" + Values+ ");";
            statement.executeUpdate(sqlInsert);
        } catch (SQLException e) {
            err.println(createSqlExceptionInfo(e));
        } finally {
            resetStatement();
        }
    }

    public void update(String sqlUpdate) {
        try {
            statement = connection.createStatement();

            statement.executeUpdate(sqlUpdate);
        } catch (SQLException e) {
            err.println(createSqlExceptionInfo(e));
        } finally {
            resetStatement();
        }
    }
    public void updateRow(ArrayList<Object> myVals, String tableName){
        try {
            statement = connection.createStatement();
            
            String columnNames = "";
            String Values = "";
            
            String sqlUpdate = "UPDATE "+tableName+" SET ";
            
            for(int i = 0 ; i < myVals.size() ; i++){
               if(fieldTypes.get(i).equals("String")){
                    Values += "'" + myVals.get(i) + "'";
                    columnNames = "`"+fieldNames.get(i)+"`";
                sqlUpdate+=columnNames+" = '"+myVals.get(i)+"'";
                  
                 
                }
                else{
                    Values += myVals.get(i);
                   columnNames = "`"+fieldNames.get(i)+"`";
                sqlUpdate+=columnNames+" = "+myVals.get(i);
                  
                }
                if(i!= myVals.size() - 1)
                {
                    sqlUpdate+=", ";
                    Values += ", ";
                    columnNames +=", ";
                }
            }
             sqlUpdate+=  " WHERE `"+fieldNames.get(1)+"` = "+myVals.get(1)+" ;";
            
          //  System.out.println(Values+"\n"+columnNames);
            statement.executeUpdate(sqlUpdate);
        } catch (SQLException e) {
            err.println(createSqlExceptionInfo(e));
        } finally {
            resetStatement();
        }
    }

    public void delete(String sqlDelete) {
        try {
            statement = connection.createStatement();

            statement.executeUpdate(sqlDelete);
        } catch (SQLException e) {
            err.println(createSqlExceptionInfo(e));
        } finally {
            resetStatement();
        }
    }

    public boolean closeConnection() {
        try {
            connection.close();

            return true;
        } catch (SQLException e) {
            err.println(createSqlExceptionInfo(e));
        }

        return false;
    }

    public static String createSqlExceptionInfo(SQLException e) {
        String ret = "SQL-State:\t" + e.getSQLState() + "\n";
        ret += "SQL error-code:\t" + e.getErrorCode() + "\n";
        ret += "Description:\t" + e.getMessage();

        return ret;
    }

    private void resetStatement() {
        if (statement != null) {
            statement = null;
        }
    }
}
