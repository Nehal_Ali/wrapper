
package m_project;
public class Student extends DbObject{
    String name;
    int studentID;
    int age; 
    Student(){};
    Student(String name, int ID, int age){
        this.name = name;
        this.studentID = ID;
        this.age = age;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setStudentID(int studentID) {
        this.studentID = studentID;
    }
    public void setAge(int age) {
        this.age = age;
    } 
    public String getName() {
        return name;
    }
    public int getstudentID() {
        return studentID;
    }
    public int getAge() {
        return age;
    }
}
